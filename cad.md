# CAD
Computer unterstütze Konstruktion (CAD, computer aided design)

## OpenSCAD
[OpenSCAD](https://openscad.org/) ist eine freie CAD-Software. Mit einer textbasierten Programmiersprache werden 3D-Modelle erzeugt. Die Objekte bestehen aus einfachen geometrischen Grundkörpern und werden mit Transformationen und Modifikationen zu einem komplexen 3D-Modell vereinigt. [Quelle](https://de.wikipedia.org/wiki/OpenSCAD)

- customizer  
  Mit dem Fenster Customizer können vorgegebene Eigenschaften angepasst werden.
- Texteditor  
  Die Skriptdateien mit der Dateiendung .scad könne mit beliebigen Texteditoren bearbeitet weren.
- F6 Render  
  Fertigstellung der Ausgabe  
- F7 Export as STL  
  Zur Weiterverarbeitung für den 3D-Druck wird die Ausgabe im STL-Format gespeichert.
- Befehlszeile  
	- man openscad  
      Unter Linux wird mit diesem Befehl das Handbuch ausgegeben
    - openscad -o out.stl in.scad -q
	  Aufruf von OpenSCAD zum Rendern der Eingabedatei in.scad in eine Ausgabedatei out.stl

## viewer, slicer
- plater out.stl &
- PrusaSlicer-2.5.0+linux-x64-GTK2-202209060714.AppImage out.stl &
- slic3r --info out.stl
