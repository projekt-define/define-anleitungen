# Shell
Die Unix-Shell oder kurz Shell (englisch für Hülle, Schale) bezeichnet die traditionelle Benutzerschnittstelle unter Unix oder unixoiden Computer-Betriebssystemen. Der Benutzer kann in einer Eingabezeile Kommandos eintippen, die der Computer dann sogleich ausführt. Man spricht darum auch von einem Kommandozeileninterpreter. [Quelle](https://de.wikipedia.org/wiki/Unix-Shell)

Windows-Eingabeaufforderung cmd.exe ist eine Betriebssystem-Shell von Windows. In der Shell können DOS-Kommandozeilenbefehle verarbeitet und Stapelverarbeitungsdateien ausgeführt werden. [Quelle](https://de.wikipedia.org/wiki/Cmd.exe)

# SSH
Secure Shell oder SSH bezeichnet ein kryptographisches Netzwerkprotokoll für den sicheren Betrieb von Netzwerkdiensten über ungesicherte Netzwerke. Häufig wird es verwendet, um lokal eine entfernte Kommandozeile verfügbar zu machen, d. h., auf einer lokalen Konsole werden die Ausgaben der entfernten Konsole ausgegeben, und die lokalen Tastatureingaben werden an den entfernten Rechner gesendet. [Quelle](https://de.wikipedia.org/wiki/Secure_Shell)  
-   SSH für Windows [Putty](https://putty.org/)


# Screenreader


## Befehlsezeileneingabe mit Screenreadern

1.  Eingabeaufforderung Tastaturkürzel

    -   Öffnen und Schließen  
        -   Windows (oder Windows+R) und geben Sie dann "cmd" ein:  Führen Sie die Eingabeaufforderung im normalen Modus aus.
        -   Win+X und dann C drücken: Führt die Eingabeaufforderung im normalen Modus aus. (Neu in Windows 10)
        -   Win+X und dann A drücken: Führen Sie die Eingabeaufforderung mit administrativen Rechten aus. (Neu in Windows 10)
        -   Alt+F4 (oder geben Sie an der Eingabeaufforderung "exit" ein): Schließen Sie die Eingabeaufforderung.
    -   Bewegen  
        -   Alt+Eingabe: Umschalten zwischen Vollbildmodus und Fenstermodus.
        -   Anfang/Ende: Bewegen Sie die Einfügemarke an den Anfang bzw. das Ende der aktuellen Zeile.
        -   Strg+Pfeil links/rechts: Bewegen Sie die Einfügemarke an den Anfang des vorherigen bzw. nächsten Worts in der aktuellen Zeile.
        -   Strg+Pfeil nach oben/unten: Blättern Sie auf der Seite nach oben oder unten, ohne die Einfügemarke zu verschieben.
        -   Strg+M: Markierungsmodus aufrufen oder beenden. Im Markierungsmodus können Sie alle vier Pfeiltasten verwenden, um den Cursor im Fenster zu bewegen. Beachten Sie, dass Sie die Pfeiltasten Links und Rechts immer verwenden können, um Ihre Einfügemarke in der aktuellen Zeile nach links oder rechts zu bewegen, unabhängig davon, ob der Markierungsmodus aktiviert oder deaktiviert ist.
    -   Auswählen  
        -   Strg+A: Markiert den gesamten Text in der aktuellen Zeile. Drücken Sie erneut Strg+A, um den gesamten Text im CMD-Puffer auszuwählen.
        -   Umschalttaste+Pfeil links/Pfeil rechts: Erweitert die aktuelle Auswahl um ein Zeichen nach links oder rechts.
        -   Umschalt+Strg+Pfeil links/Pfeil rechts: Erweitern der aktuellen Auswahl um ein Wort nach links oder rechts.
        -   Umschalttaste+Pfeil nach oben/Pfeil nach unten: Erweitert die aktuelle Auswahl um eine Zeile nach oben oder unten. Die Auswahl wird bis zu der Position in der vorherigen oder nächsten Zeile erweitert, die der Position der Einfügemarke in der aktuellen Zeile entspricht.
        -   Umschalt+Home: Erweitert die aktuelle Auswahl bis zum Anfang eines Befehls. Drücken Sie erneut Umschalt+Home, um den Pfad (z. B. C:\Windows\system32) in die Auswahl aufzunehmen.
        -   Umschalt+Ende: Erweitert die aktuelle Auswahl bis zum Ende der aktuellen Zeile.
        -   Strg+Umschalt+Start/Ende: Erweitern der aktuellen Auswahl bis zum Anfang bzw. Ende des Bildschirmpuffers.
        -   Umschalt+Seite hoch/Seite runter: Erweitert die aktuelle Auswahl um eine Seite nach oben oder unten.
    -   Bearbeiten  
        -   Strg+C (oder Strg+Einfügen): Kopiert den aktuell ausgewählten Text. Beachten Sie, dass dies nur funktioniert, wenn Sie einen Text ausgewählt haben. Ist dies nicht der Fall, bricht Strg+C den aktuellen Befehl ab (den wir in Kürze genauer beschreiben).
        -   F2 und dann ein Buchstabe: Kopiert den Text rechts von der Einfügemarke bis zu dem Buchstaben, den Sie eingegeben haben.
        -   Strg+V (oder Umschalt+Einfügen): Text aus der Zwischenablage einfügen.
        -   Rücktaste: Löscht das Zeichen links von der Einfügemarke.
        -   Strg+Rücktaste: Löscht das Wort links von der Einfügemarke.
        -   Tabulator: Automatisches Vervollständigen eines Ordnernamens.
        -   Escape: Löscht die aktuelle Textzeile.
        -   Einfügen: Schaltet den Einfügemodus um. Wenn der Einfügemodus aktiviert ist, wird alles, was Sie eingeben, an der aktuellen Position eingefügt. Wenn er ausgeschaltet ist, überschreibt alles, was Sie eingeben, den bereits vorhandenen Text.
        -   Strg+Home/End: Löscht Text von der Einfügemarke bis zum Anfang oder Ende der aktuellen Zeile.
        -   Strg+Z: Markiert das Ende einer Zeile. Text, den Sie nach diesem Punkt in dieser Zeile eingeben, wird ignoriert.
    -   Historie  
        -   F3: Wiederholung des vorherigen Befehls.
        -   Pfeil nach oben/unten: Blättern Sie rückwärts und vorwärts durch die vorherigen Befehle, die Sie in der aktuellen Sitzung eingegeben haben. Sie können auch F5 anstelle des Pfeils nach oben drücken, um rückwärts durch den Befehlsverlauf zu blättern.
        -   Pfeil nach rechts (oder F1): Stellen Sie den vorherigen Befehl Zeichen für Zeichen wieder her.
        -   F7: Zeigt einen Verlauf der vorherigen Befehle an. Sie können mit den Pfeiltasten nach oben/unten einen beliebigen Befehl auswählen und dann die Eingabetaste drücken, um den Befehl auszuführen.
        -   Alt+F7: Löscht die Befehlshistorie.
        -   F8: Geht im Befehlsverlauf zu den Befehlen zurück, die dem aktuellen Befehl entsprechen. Dies ist nützlich, wenn Sie einen Teil eines Befehls eingeben möchten, den Sie bereits mehrmals verwendet haben, und dann in Ihrem Verlauf zurückblättern, um genau den Befehl zu finden, den Sie wiederholen möchten.
        -   Strg+C: Bricht die aktuelle Zeile, die Sie gerade tippen, oder einen Befehl, der gerade ausgeführt wird, ab. Beachten Sie, dass dieser Befehl die Eingabe einer Zeile nur dann abbricht, wenn Sie keinen Text markiert haben. Wenn Sie Text markiert haben, wird der Text stattdessen kopiert.

2.  NVDA

    1.  Die NVDA-Modifikatortaste  
        NVDA kann so konfiguriert werden, dass die Einfügetaste, die erweiterte Einfügetaste und/oder die Feststelltaste als NVDA-Modifikatortaste verwendet werden kann. Standardmäßig sind sowohl die Einfügetaste des Ziffernblocks als auch die erweiterte Einfügetaste als NVDA-Modifikatortasten eingestellt.
    2.  [Text überprüfen](https://www.nvaccess.org/files/nvda/documentation/userGuide.html?#ReviewingText)  
        Mit NVDA können Sie den Inhalt des Bildschirms, des aktuellen Dokuments oder des aktuellen Objekts zeichen-, wort- oder zeilenweise lesen. Dies ist vor allem an Stellen nützlich (einschließlich Windows-Befehlskonsolen), an denen es kein System-Caret gibt. Zum Beispiel können Sie damit den Text einer langen Informationsmeldung in einem Dialogfeld überprüfen.  
        Wenn Sie den Überprüfungscursor bewegen, folgt das Systemcursorzeichen nicht, sodass Sie den Text überprüfen können, ohne Ihre Bearbeitungsposition zu verlieren. Standardmäßig folgt der Überprüfungscursor jedoch der Bewegung des Systemcursors. Dies kann ein- und ausgeschaltet werden.  
        Die folgenden Befehle sind für die Überprüfung von Text verfügbar:
    3.  Desktop-Tastenbelegung  
        Für die Verwendung des Ziffernblocks mit NVDA muss der Ziffernblock mit der Ziffernblocktaste (Num) ausgeschalten sein.  
        1.  Zur vorherigen Zeile in der Überprüfung springen  
            Ziffernblock 7
        2.  Aktuelle Zeile in der Überprüfung anzeigen  
            Ziffernblock 8
        3.  Wechseln zur nächsten Zeile in der Überprüfung  
            Ziffernblock 9
        4.  Zum vorherigen Wort in der Überprüfung springen  
            Ziffernblock 4
        5.  Aktuelles Wort in der Überprüfung melden  
            Ziffernblock 5
        6.  Zum nächsten Wort in der Überprüfung wechseln  
            Ziffernblock 6
    4.  Laptop-Tastenbelegung  
        1.  Zur vorherigen Zeile in der Überprüfung springen  
            NVDA + Pfeil nach oben
        2.  Aktuelle Zeile in der Überprüfung melden  
            NVDA + Umschalttaste + Punkt
        3.  Wechseln zur nächsten Zeile in der Überprüfung  
            NVDA + Pfeil abwärts
        4.  Zum vorherigen Wort in der Überprüfung springen  
            NVDA + Steuerung + linker Pfeil
        5.  Aktuelles Wort in der Übersicht anzeigen  
            NVDA + Steuerung + Punkt
        6.  Zum nächsten Wort in der Überprüfung springen  
            NVDA + Steuerung + Pfeil rechts

3.  JAWS

    1.  [Tastenanschläge](https://support.freedomscientific.com/Content/Documents/Manuals/JAWS/Keystrokes.txt)
    2.  Desktop-Tastenanbelegung  
        Zum Lesen von Text  
        1.  Vorherige Zeile sagen  
            Pfeil nach oben
        2.  Zeile sagen  
            Einfügen + Pfeil nach oben
        3.  Nächste Zeile sagen  
            Pfeil Runter
        4.  Vorheriges Wort sagen  
            Einfügen + Pfeil nach links
        5.  Wort sagen  
            Einfügen + Zahlenblock 5
        6.  Nächstes Wort sagen  
            Einfügen + Pfeil nach rechts
    3.  Laptop-Tastenbelegung  
        Zum Lesen von Text  
        1.  Vorherige Zeile sagen  
            Feststelltaste + J
        2.  Zeile sagen  
            Feststelltaste + I
        3.  Nächste Zeile sagen  
            Feststelltaste + O
        4.  Vorheriges Wort sagen  
            Feststelltaste + J
        5.  Wort sagen  
            Feststelltaste + K
        6.  Nächstes Wort sagen  
            Feststelltaste + L

4.  Orca

    1.  [Orca ScreenReader](https://help.gnome.org/users/orca/stable/index.html.de)
    2.  Screen Reader Modifier Taste(n)  
        Einfügen, Ziffernblock_Einfügen
    3.  Tastenbelegung Desktop  
        1.  Stummschaltung der Sprache ein- und ausschalten  
            Einfügen + s
        2.  Flat Review an den Anfang der vorherigen Zeile verschieben  
            Ziffernblock Pos1
        3.  Sprechen der aktuellen Zeile der Wohnungsbesprechung  
            Ziffernblock Aufwärts
        4.  Wohnungsbesprechung an den Anfang der vorherigen Zeile verschieben  
            Ziffernblock Seite oben
        5.  Verschieben der flachen Übersicht zum vorherigen Element oder Wort  
            Ziffernblock Links
        6.  Verschieben der flachen Übersicht zum nächsten Element oder Wort  
            Ziffernblock Rechts


## NVDA

-   [NVDA-Screenreader](https://www.nvaccess.org/)
-   Navigating with NVDA  
    -   The NVDA Modifier Key  
        NVDA can be configured so that the numpad Insert, Extended Insert and/or Caps Lock key can be used as the NVDA modifier key. By default, both the numpad Insert and Extended Insert keys are set as NVDA modifier keys.
    -   Navigating with the System Focus  
        httppassts://www.nvaccess.org/files/nvda/documentation/userGuide.html?#SystemFocus  
        The system focus, also known simply as the focus, is the object which receives keys typed on the keyboard. For example, if you are typing into an editable text field, the editable text field has the focus.
    -   Navigating with the System Caret  
        <https://www.nvaccess.org/files/nvda/documentation/userGuide.html?#SystemCaret>  
        When an object that allows navigation and/or editing of text is focused, you can move through the text using the system caret, also known as the edit cursor.
    -   [X] Reviewing Text  
        <https://www.nvaccess.org/files/nvda/documentation/userGuide.html?#ReviewingText>  
        NVDA allows you to read the contents of the screen, current document or current object by character, word or line. This is mostly useful in places (including Windows command consoles) where there is no system caret. For example, you might use it to review the text of a long information message in a dialog.  
        When moving the review cursor, the System caret does not follow along, so you can review text without losing your editing position. However, by default, when the System caret moves, the review cursor follows along. This can be toggled on and off.  
        The following commands are available for reviewing text:  
        -   Desktop keys  
            -   Move to previous line in review  
                numpad7
            -   Report current line in review  
                numpad8
            -   Move to next line in review  
                numpad9
            -   Move to previous word in review  
                numpad4
            -   Report current word in review  
                numpad5
            -   Move to next word in review  
                numpad6
        -   Laptop keys  
            -   Move to previous line in review  
                NVDA+upArrow
            -   Report current line in review  
                NVDA+shift+.
            -   Move to next line in review  
                NVDA+downArrow
            -   Move to previous word in review  
                NVDA+control+leftArrow
            -   Report current word in review  
                NVDA+control+.
            -   Move to next word in review  
                NVDA+control+rightArrow
-   In NVDA screen reader, you can use command prompt with the review text key shortcuts.  
    for all the keystrokes used within text review, refer nvda commands quick reference by navigating to nvda menu>;help>;commands quick reference>;Reviewing Text heading.  
    -   <https://pwvi.org/using-command-prompt-with-nvda/>
-   NVDA besitzt ein Plugin, welches die Sprache automatisch richtig erkennt,  
    "Plugin that detects any Language" im NVDA Store  
    es verwendet openAI LAMA  
    das könnten für die Befehlszeile zum Sprache umschalten verwenden  
-   NVDA Befehlszeileneingabe gut lesbar mit "Virtuelle Ansicht". Da können die Pfeiltasten zur Navigation verwendet werden.  

## JAWS

-   [JAWS-ScreenReader](https://www.freedomscientific.com/products/software/home-licenses/)
-   using a command prompt with JAWS  
    <https://www.reddit.com/r/Blind/comments/8zf1f1/using_a_command_prompt_with_jaws/>  
    -   Using numpad minus gets you to the JAWS cursor,
    -   Use JAWS key+numpad minus to root the JAWS cursor to the PC cursor
    -   If you want the text of the window in a virtual buffer, you can do this with JAWS key+alt+W.
    -   Using numpad plus gets you to the PC-cursor,
-   Keystrokes  
    <https://support.freedomscientific.com/Content/Documents/Manuals/JAWS/Keystrokes.txt>  
    -   Desktop Keystrokes  
        For Reading Text  
        -   Say Prior Line  
            UP ARROW
        -   Say Line  
            INSERT+UP ARROW
        -   Say Next Line  
            DOWN ARROW
        -   Say Prior Word  
            INSERT+LEFT ARROW
        -   Say Word  
            INSERT+NUM PAD 5
        -   Say Next Word  
            INSERT+RIGHT ARROW
    -   Laptop Keystrokes  
        For Reading Text  
        -   Say Prior Line  
            CAPS LOCK+J
        -   Say Line  
            CAPS LOCK+I
        -   Say Next Line  
            CAPS LOCK+O
        -   Say Prior Word  
            CAPS LOCK+J
        -   Say Word  
            CAPS LOCK+K
        -   Say Next Word  
            CAPS LOCK+L
-   Tastenanschläge  
    <https://support.freedomscientific.com/Content/Documents/Manuals/JAWS/Keystrokes.txt>  
    -   Desktop-Tastenanschläge  
        Zum Lesen von Text  
        -   Vorherige Zeile sagen  
            PFEIL OBEN
        -   Zeile sagen  
            EINFÜGEN+PFEIL NACH OBEN
        -   Nächste Zeile sagen  
            PFEIL RUNTER
        -   Vorheriges Wort sagen  
            EINFÜGEN+PFEIL NACH LINKS
        -   Wort sagen  
            EINFÜGEN+ZAHLENBLOCK 5
        -   Nächstes Wort sagen  
            EINFÜGEN+PFEIL NACH RECHTS
    -   Laptop-Tastenanschläge  
        Zum Lesen von Text  
        -   Vorherige Zeile sagen  
            Feststelltaste+J
        -   Zeile sagen  
            Feststelltaste+I
        -   Nächste Zeile sagen  
            Feststelltaste+O
        -   Vorheriges Wort sagen  
            Feststelltaste+J
        -   Wort sagen  
            Feststelltaste+K
        -   Nächstes Wort sagen  
            Feststelltaste+L


## Orca

-   [Orca ScreenReader](https://help.gnome.org/users/orca/stable/index.html.de)
-   Einstellungen  
    -   orca [-s|&#x2013;setup]  
        Orca-Zusatztaste + Leertaste
-   Screen Reader Modifier Key(s)  
    Insert, KP_Insert
-   Key Bindings  
    -   Toggle the silencing of speech  
        Insert+s
    -   Move flat review to the beginnung of the previous line  
        KP_Home
    -   Speak the current flat review line  
        KP_Up
    -   Move flat review to the beginnung of the previous line  
        KP_Page_Up
    -   Move flat review to the previous item or word  
        KP_Left
    -   Move flat review to the next item or word  
        KP_Right
-   Screen Reader Modifier Taste(n)  
    Einfügen, KP_Einfügen
-   Tastenbelegung  
    -   Stummschaltung der Sprache ein- und ausschalten  
        Einfügen+s
    -   Flat Review an den Anfang der vorherigen Zeile verschieben  
        KP_Haushalt
    -   Sprechen der aktuellen Zeile der Wohnungsbesprechung  
        KP_Aufwärts
    -   Wohnungsbesprechung an den Anfang der vorherigen Zeile verschieben  
        KP_Seite_oben
    -   Verschieben der flachen Übersicht zum vorherigen Element oder Wort  
        KP_Links
    -   Verschieben der flachen Übersicht zum nächsten Element oder Wort  
        KP_Rechts
-   funktioniert im Gnome Terminal  
    nicht in Emacs
