# Define Anleitungen (Instructions)
Project Define makes the maker movement accessible to blind and visually impaired people.
This instructions are based on our approach, our experiences and suggestions for subsequent projects.

# Contact
[https://defineblind.at](https://defineblind.at)

[Mail: Info Define info_at_defineblind.at](mailto:Info Define <info_at_defineblind.at>)

# Attribution
BSV-WNB/Define

[https://defineblind.at](https://defineblind.at)

# Acknolwedgement
This work was developed as part of the [Define](https://defineblind.at), [Mail: Info Define info_at_defineblind.at](mailto:Info Define <info_at_defineblind.at>) entwickelt.

The supporting organization for the Define project is the [Blinden- und Sehbehindertenverband Wien, Niederösterreich und Burgenland](https://www.blindenverband-wnb.at/) (BSVWNB). 


This project is funded by [AK Vienna within the framework oft the Digitization Fund Work 4.0](https://wien.arbeiterkammer.at/service/digifonds/index.html).

This project is based on the work of [Careables Training Kit](https://www.careables.org/resource/careables-training-kit/) von [carebles.org](careables.org).

# Copyright & License
    SPDX-FileCopyrightText: 2023 BSV-WNB/Define <info_at_defineblind.at>
	SPDX-License-Identifier: CC-BY-SA-4.0

Define Leitfaden © 2023 by [BSV-WNB/Define](https://defineblind.at) is licensed under [CC-BY-SA 4.0 Attribution-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/).
