# Anleitung blinder 3D Druck

## Vorraussetzungen


### Verwendete Software

-   Windows 10
-   Screenreader NVDA
-   Putty
-   PrusaSlicer
-   Octoprint
-   Printrun


### Verwendete Hardware

-   Rechner mit Windows 10
-   Raspberry Pi A, A+, B, B+, 2B, 3A+, 3B, 3B+, 4B 1/2/4GB  
    mit Netzteil und Micro-SD-Card (8 GB Kapazität oder größer) für Octopi  
    Wir haben Raspberry Pi 3B verwendet.
-   Prusa i3 oder Prusa MINI  
    Wir haben Prusa i3 MK2.5S verwendet.
-   USB-Kabel  
    -   USB-Typ-A-Kabel zur Verbindung von Raspberry Pi 3B mit Prusa i3
    -   Micro USB Kabel zur Verbindung von Raspberry Pi 3B mit Prusa MINI
-   WiFi Netzwerk


## Vorbereitung

-   Windows  
    -   NVDA auf Windows installieren  
        <https://www.nvaccess.org/download/>
    -   PrusaSlicer installieren  
        <https://www.nvaccess.org/download/>
    -   Putty auf Windows installieren  
        <https://putty.org/>
-   Raspberry PI  
    -   Installiere Octoprint auf der Micro-SD-Card für den Raspberry Pi 3B,  
        siehe Anleitung auf <https://octoprint.org/download/>  
        -   Installiere das 3D-Druck Raspberry Pi Betriebssystem Octopi mit dem Raspberry Pi Imager,  
            siehe auch <https://www.raspberrypi.com/software/>
        -   Konfigurieren unter fortgeschrittene Einstellungen  
            -   Setze den Hostnamen.  
                Wir haben "octopi.local" verwendet.
            -   Erlaube SSH mit Passwort Authentifizierung.
            -   Verwende den Benutzernamen "pi".  
                Setze das Passwort für deinen Benutzer.
            -   Hostname, Benutzername und Passwort für die spätere Verwendung bereit halten.
            -   Konfiguriere die Zugangasdaten deines WiFi Netzwerks.  
                -   Gib den SSID Namen und das Passwort für dein WiFi Netzwerk an.
            -   Du kannst das Land und die Zeitzone in dem der Rechner betrieben wird angeben
            -   Gib deine Tastaturbelegung an.
    -   Printrun installieren  
        <https://www.pronterface.com/>  
        Über eine SSH Verbindung zum Raspberry PI kann printrun über die Befehlszeile installiert werden  
        Der Befehl lautet "sudo apt install printrun". Nach der Eingabe wird das Passwort vom Benutzer verlangt.


## Durchführung


### PrusaSlicer

Wir haben die grafische Oberfläche von PrusaSlicer mit Tastaturbefehlen bedient.  
Mit dem PrusaSlicer und einem Model im STL-Dateiformat, Dateiendung "stl" wird ein G-Code für den 3D-Drucker erstellt.  
Wir gehen davon aus, dass der 3D-Drucker und das Filament eingerichtet und konfiguriert sind.  
Geladen wird das Model im STL-Dateiformat mit dem Tastaturbefehl Steuerungstaste+i.  
Geschnitten wir diese Model im PrusaSlicer mit dem Tastaturbefehl Steuerungstaste+r.  
Exportiert wird das geschnittene Model im PrusaSlicer mit dem Tastaturbefehl Steuerungstaste+g in das G-Code-Dateiformat mit der Dateiendung "gcode".  


### Befehlszeile

-   Die Befehlszeile kann mit der Windowstast+r, der anschließenden Eingabe "cmd" und Bestätigung mit Enter geöffnet werden.
-   Bemerkungen zur Befehlszeile  
    -   Befehle werden in der untersten Zeile eingegeben. Die Zeile für die Befehlseingabe beginnt unter Windows mit dem aktuellen Verzeichnis-Pfad und dem Zeichen für spitze Klammer zu ">". Dahinter erfolgt die Eingabe der Befehle, welche mit der Taste "Enter" bestätigt wird. Die Ausgabe der eingegebenen Befehle erfolgt unter der Befehlseingabezeile. Ist die Ausgabe der Befehle beendet, erscheint eine neue Befehlseingabezeile als unterste Zeile. Die Befehlszeile verwendet die englische Sprache.
    -   Die Tabulator-Taste zeigt mögliche Dateinamen basierend auf der bereits erfolgten Eingabe an oder vervollständigt angefangene Dateinamen, die eindeutig zuordenbar sind.
    -   Die Pfeiltasten nach Oben und Unten rufen bereits eingegebenen Befehle erneut auf.
-   Wichtige Befehle  
    -   Der Befehl "help" zeigt mögliche Befehle an.
    -   Der Befehl "dir" ist die Abkürzung für Directory und zeigt den Inhalt des aktuellen Verzeichnis an, wenn der Befehl alleine verwendet wird.  
        Der Inhalt eines Unterverzeichnisses vom aktuellen Verzeichnis wird angezeigt, wenn der Unterverzeichnisname durch ein Leerzeichen vom Befehl "dir" getrennt vor der Bestätigung mit "Enter" angegeben wird.  
        Der Inhalt eines Verzeichnisses wird angegeben, wenn der Pfad zum Verzeichnis durch ein Leerzeichen vom Befehl "dir" getrennt vor der Bestätigung mit "Enter" angegeben wird.
    -   Der Befehl "cd" ist die Abkürzung für ChangeDirectory und wird zum Wecheln des aktuellen Verzeichnisses verwendet.  
        Der Befehl "cd" wechselt in ein Unterverzeichnis, wenn der Unterverzeichnisname durch ein Leerzeichen vom Befehl "dir" getrennt vor der Bestätigung mit "Enter" angegeben wird.  
        Der Befehl "cd" wechselt in ein Verzeichnis, wenn der Pfad zum Verzeichnis durch ein Leerzeichen vom Befehl "dir" getrennt vor der Bestätigung mit "Enter" angegeben wird.
-   Navigation mit NVDA in der Befehlszeile  
    1.  [Text überprüfen](https://www.nvaccess.org/files/nvda/documentation/userGuide.html?#ReviewingText)  
        Mit NVDA können Sie den Inhalt des Bildschirms, des aktuellen Dokuments oder des aktuellen Objekts zeichen-, wort- oder zeilenweise lesen. Dies ist vor allem an Stellen nützlich (einschließlich Windows-Befehlskonsolen), an denen es kein System-Caret gibt. Zum Beispiel können Sie damit den Text einer langen Informationsmeldung in einem Dialogfeld überprüfen.  
        Wenn Sie den Überprüfungscursor bewegen, folgt das Systemcursorzeichen nicht, sodass Sie den Text überprüfen können, ohne Ihre Bearbeitungsposition zu verlieren. Standardmäßig folgt der Überprüfungscursor jedoch der Bewegung des Systemcursors. Dies kann ein- und ausgeschaltet werden.  
        Die folgenden Befehle sind für die Überprüfung von Text verfügbar:
    2.  Desktop-Tastenbelegung  
        Für die Verwendung des Ziffernblocks mit NVDA muss der Ziffernblock mit der Ziffernblocktaste (Num) ausgeschalten sein.  
        1.  Zur vorherigen Zeile in der Überprüfung springen  
            Ziffernblock 7
        2.  Aktuelle Zeile in der Überprüfung anzeigen  
            Ziffernblock 8
        3.  Wechseln zur nächsten Zeile in der Überprüfung  
            Ziffernblock 9
        4.  Zum vorherigen Wort in der Überprüfung springen  
            Ziffernblock 4
        5.  Aktuelles Wort in der Überprüfung melden  
            Ziffernblock 5
        6.  Zum nächsten Wort in der Überprüfung wechseln  
            Ziffernblock 6
    3.  Laptop-Tastenbelegung  
        1.  Zur vorherigen Zeile in der Überprüfung springen  
            NVDA-Taste + Pfeil nach oben
        2.  Aktuelle Zeile in der Überprüfung melden  
            NVDA-Taste + Umschalttaste + Punkt
        3.  Wechseln zur nächsten Zeile in der Überprüfung  
            NVDA-Taste + Pfeil abwärts
        4.  Zum vorherigen Wort in der Überprüfung springen  
            NVDA-Taste + Steuerung + linker Pfeil
        5.  Aktuelles Wort in der Übersicht anzeigen  
            NVDA-Taste + Steuerung + Punkt
        6.  Zum nächsten Wort in der Überprüfung springen  
            NVDA-Taste + Steuerung + Pfeil rechts


### Verbindung zu entfernten Rechnern

-   Ist die Software Putty installiert, stehen die Befehle SSH, Abkürzung für Secure Shell, und SCP, Abkürzung für Secure Copy zur Verbindung mit SSH-Servern zur in der Befehlszeile zur Verfügung.
-   Mit dem Befehl "scp" eine lokale Datei zu einem SSH-Server übertragen.  
    Um eine Datei im aktuellen Verzeichnis in ein Benutzerverzeichnis auf einem SSH-Server zu übertragen, wird hinter scp durch ein Leerzeichen getrennt der Dateiname geschrieben.  
    Benutzer und SSH-Server-Name zu dem Übertragen werden soll, wird durch ein "@", wie bei einer E-Mail-Adresse, getrennt angegeben.  
    Durch ein Leerzeichen getrennt wir der Benutzer + "@" + SSH-Server-Name gefolgt von einem Doppelpunkt ":" hinter den Dateinamen geschrieben und mit Enter bestätigt.  
    Bei unbekanntem Fingerabdruck des SSH-Servers wird um die Bestätigung mit der Eingabe von "yes" und Eingabe mit "Enter" gebeten.  
    Anschließend ist das Passwort vom Benutzer auf dem SSH-Server mit Enter zu bestätigen.  
    Zum Beispiel wir im Laufwerk C: die Datei modell.gcode zum Benutzer "pi" auf dem SSH-Server octopi.local mit folgendem Befehl übertragen.  
    C:> scp modell.gcode pi@octopi.local:  
    In unserem Beispiel können wir so die von PrusaSlice exportierten G-Code Dateien zum Octopi übertragen und verwenden dazu die Zugangsdaten von der Konfiguration des Octopi.
-   Mit SSH die Befehlszeile auf SSH-Servern bedienen.  
    Die Befehlszeile eines Benutzers auf einem SSH-Server wird mit dem Befehl "ssh" gefolgt von einem Leerzeichen, Benutzername, "@" Zeichen und dem SSH-Server-Namen und "Enter" gestartet.  
    Bei unbekanntem Fingerabdruck des SSH-Servers wird um die Bestätigung mit der Eingabe von "yes" und Eingabe mit "Enter" gebeten.  
    Anschließend ist das Passwort vom Benutzer auf dem SSH-Server mit Enter zu bestätigen.  
    Die Befehlszeile des Benutzers am SSH-Servers meldet sich bei erfolgreicher Verbindung.  
    Je nach Benutzer und SSH-Server können sich die Befehlzeilen unterscheiden.  
    Beispiel einer Verdingung über eine Secure Shell zu dem SSH-Server "octopi.local" mit dem Benutzer "pi".  
    C:> ssh pi@octopi.local  
    pi@octopi.local's password:  
    pi@octopi:~ $  
    In unserem Beispiel verwenden wir dazu die Zugangsdaten von der Konfiguration des Octopi.
-   Ob ein entfernter Rechner erreichbar ist, kann mit dem Ping-Befehl untersucht werden.  
    Antwortet ein entfernter Rechner auf einen Ping-Befehl ist er meistens auch über weitere Dienste erreichbar.  
    Der Ping-Befehl sendet Datenpakete an den entfernten Rechner, die zurückgeschickt werden.  
    Der Ping-Befehl berichtet, wie viele der gesendeten Paket in welcher Zeit zurückkommen.  
    Kommen alle Paket in kurzer Zeit zurück, ist die Verbindung zum entfernten Rechner gut.  
    Um die Erreichbarkeit des Rechners octopi.local zu prüfen, wird folgender Befehl mit Enter bestätigt.  
    C:> ping octopi.local  
    Ist der Rechner erreichbar werden die letzten Ausgabezeilen des Ping-Befehls in etwas so aussehen  
    &#x2014; 1.1.1.1 ping statistics &#x2014;  
    4 packets transmitted, 4 received, 0% packet loss, time 3004ms  
    rtt min/avg/max/mdev = 7.975/10.123/13.248/2.068 ms  
    Ist der Rechner nicht erreichbar werden die letzten Ausgabezeilen des Ping-Befehls in etwas so aussehen  
    &#x2014; 254.1.1.1 ping statistics &#x2014;  
    21 packets transmitted, 0 received, 100% packet loss, time 20475ms


### Pronsole

Damit wir den 3D-Drucker mit der pronsole steuern können verbinden wir den Octopi mit dem 3D-Drucker über ein USB-Kabel und verbinden uns über das WiFi Netzwerk mit SSH.  

-   Vorbereitungen vor dem 3D-druck mit der pronsole  
    1.  Octopi und 3D-Drucker sind über USB-Kabel verbinden und beide einschalten.
    2.  Unseren Windows Rechner mit dem gleichen WiFi Netzwerk wie Octopi verbinden.
    3.  G-Code vom PrusaSlicer zum Octopi mit SCP übertragen.
    4.  Befehlszeile vom Octopi mit SSH aufrufen.
    5.  Installieren der pronsole mit dem Paket printrun in der Befehlszeile über das Internet mit dem Befehl "sudo apt install printrun". Der Befehl benötigt eine Passworteingabe.
    6.  Start der pronsole mit dem Befehl "pronsole"
-   Bedienung der pronosle  
    1.  Pronsole mit dem 3D-Drucker verbinden.  
        Ohne Verbindung zum 3D-Drucker lautet die Befehlszeile der pronsole "offline>". Der Befehl "connect" stellt eine Verbindung von der pronsole zum 3D-Drucker her, dabei ändert sich die Befehlszeile zu "ttyACM0 22°>". Die Befehlzeile beginnt bei einer aktiven Verbindung mit dem verbundenen Port, Anschluss und der Temperatur des Druckkopfs gefolgt von "°> "
    2.  G-Code wird mit dem Befehl "load" zum 3D-Druck geladen.  
        Der Befehl "load" gefolgt von Leerzeichen, Dateinamen einer Datei im G-Code Dateiformat und Enter bereitet den Druck vor und zeigt die geschätzte Druckdauer an.  
        Die Tabulator-Taste zeigt mögliche Dateinamen basierend auf der bereits erfolgten Eingabe an oder vervollständigt einen bereits eindeutigen Dateinamenanfang.
    3.  Der Befehl "print" start den 3D-Druck der geladenen Datei.  
        Die Befehlszeile zeigt während dem Druck zusätzlich zur Temperatur des Druckkopfs auch noch den Fortschritt in Prozent an.  
        Jede Befehlseingabe und auch nur die Enter-Taste aktualisiert die Befehlszeile.
    4.  Der Befehl "help" gibt einen Überblick über die möglichen Befehle in der pronsole


### Beenden

-   Die pronsole, die Befehlzeile am entfernten Rechner und am eigenen Rechner kann jeweils mit dem Befehl "exit" beendet werden.
-   Der Befehl "sudo poweroff" in der Befehlszeile von Octopi schaltet den Octopi ab, benötigt eine Passworteingabe und sollte vor der Unterbrechung der Stromzufuhr ausgeführt werden, um Schaden an der SD-Card zu vermeiden.


